package wsz.rpc.provider.server;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import wsz.rpc.provider.anno.RpcService;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wsz
 * @date 2021/11/30 17:16
 **/
@Service
@ChannelHandler.Sharable
public class RpcServerHandler extends SimpleChannelInboundHandler<String> implements ApplicationContextAware {

    private static final Map SERVICE_INSTANCE_MAP = new ConcurrentHashMap();

    /**
     * 通道读取就绪事件
     * @param ctx
     * @param msg
     * @throws Exception
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
//        JSON.paseObject(msg, RpcRequest.class);
    }

    /**
     * 将有@RpcService注解的bean缓存
     * @param applicationContext
     * @throws BeansException
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        Map<String, Object> beans = applicationContext.getBeansWithAnnotation(RpcService.class);
        if (beans == null || beans.size() == 0) {
            return;
        }

        Set<Map.Entry<String, Object>> entrySet = beans.entrySet();
        for (Map.Entry<String, Object> item : entrySet) {
            Object serviceBean = item.getValue();
            Class<?>[] interfaces = serviceBean.getClass().getInterfaces();
            if (interfaces.length == 0) {
                throw new RuntimeException("服务必须实现接口");
            }
            String beanName = interfaces[0].getName();
            SERVICE_INSTANCE_MAP.put(beanName, serviceBean);
        }
    }
}
