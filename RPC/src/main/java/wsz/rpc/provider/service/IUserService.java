package wsz.rpc.provider.service;

import wsz.rpc.provider.User;

/**
 * @author wsz
 * @date 2021/11/30 17:12
 **/
public interface IUserService {
    User getById(int id);
}
