package wsz.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import wsz.springboot.netty.NettyWebSocketServer;

@SpringBootApplication
public class SpringbootApplication implements CommandLineRunner {

    @Autowired
    NettyWebSocketServer nettyWebSocketServer;

    public static void main(String[] args) {
        SpringApplication.run(SpringbootApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        new Thread(nettyWebSocketServer).run();
    }
}
