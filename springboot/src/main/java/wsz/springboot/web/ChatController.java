package wsz.springboot.web;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ChatController {

    @RequestMapping("/chat")
    public String chat() {
        return "chat";
    }

    @ResponseBody
    @GetMapping(value = "tt")
    public String test() {
        return "Aaaaa";
    }
}