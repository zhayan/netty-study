package wsz.springboot.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author wsz
 * @date 2021/11/30 15:36
 **/
@Data
@Component
@ConfigurationProperties(prefix = "netty")
public class NettyConfig {

    private int port;

    private String path;
}
