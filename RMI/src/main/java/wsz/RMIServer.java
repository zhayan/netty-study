package wsz;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * @author wsz
 * @date 2021/11/30 16:52
 **/
public class RMIServer {

    public static void main(String[] args) throws RemoteException {
        Registry registry = LocateRegistry.createRegistry(8080);

        IUserService userService = new UserServiceImpl();
        registry.rebind("userService", userService);
        System.out.println("--RMI服务端启动成功--");
    }
}
