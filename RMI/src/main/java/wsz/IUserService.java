package wsz;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author wsz
 * @date 2021/11/30 16:54
 **/
public interface IUserService extends Remote {
    User getById(int id) throws RemoteException;
}
