package wsz;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

/**
 * @author wsz
 * @date 2021/11/30 16:55
 **/
public class UserServiceImpl extends UnicastRemoteObject implements IUserService{

    Map<Integer, User> userMap = new HashMap<>();

    protected UserServiceImpl() throws RemoteException {
        super();
        User user1 = new User();
        user1.setId(1);
        user1.setName("张三");
        User user2 = new User();
        user2.setId(2);
        user2.setName("李四");
        userMap.put(user1.getId(), user1);
        userMap.put(user2.getId(), user2);

    }

    @Override
    public User getById(int id) throws RemoteException {
        return userMap.get(id);
    }
}
