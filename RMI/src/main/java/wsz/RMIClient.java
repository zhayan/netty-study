package wsz;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * @author wsz
 * @date 2021/11/30 17:00
 **/
public class RMIClient {

    public static void main(String[] args) throws Exception {
        Registry registry = LocateRegistry.getRegistry("127.0.0.1", 8080);
        IUserService userService = (IUserService) registry.lookup("userService");
        User user = userService.getById(1);
        System.out.println(user);
    }
}
