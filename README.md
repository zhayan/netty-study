# nettyStudy

#### 介绍
io、nio、netty框架学习

#### 项目介绍

1. netty：基本使用，包括Handler、Decoder、Encoder、粘包/半包（暂未模拟自定义处理）
2. chat：idea中模拟聊天室
3. springboot：浏览器中模拟聊天室，前端浏览器localhost:8080/chat、后端netty=8081/chat
4. ~~NIO~~：
5. RMI：LocateRegistry
6. RPC：自定义rpc调用服务，底层使用netty
